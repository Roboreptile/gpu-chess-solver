#pragma once
#include "cpu_gpu_funcs.cuh"


void evaluate_pseudokernel(pc* boards, ushort* evals, int n) {
    for (int idx = 0; idx < n; idx++) {
        int board_start_idx = idx * NN1;
        ushort white_score = 0, black_score = 0;
        for (int i = 0; i < NN; i++) {
            switch (boards[board_start_idx + i]) {
            case king_white:
                white_score += K;
                white_score += hK_scores_w[i];
                break;
            case queen_white:
                white_score += Q;
                white_score += hQ_scores_w[i];
                break;
            case rook_white:
                white_score += R;
                white_score += hR_scores_w[i];
                break;
            case bishop_white:
                white_score += B;
                white_score += hB_scores_w[i];
                break;
            case knight_white:
                white_score += Kn;
                white_score += hK_scores_w[i];
                break;
            case pawn_white:
                white_score += P;
                white_score += hP_scores_w[i];
                break;
            case king_black:
                black_score += K;
                black_score += hK_scores_b[i];
                break;
            case queen_black:
                black_score += Q;
                black_score += hQ_scores_b[i];
                break;
            case rook_black:
                black_score += R;
                black_score += hR_scores_b[i];
                break;
            case bishop_black:
                black_score += B;
                black_score += hB_scores_b[i];
                break;
            case knight_black:
                black_score += N;
                black_score += hN_scores_b[i];
                break;
            case pawn_black:
                black_score += P;
                black_score += hP_scores_b[i];
                break;
            default:
                break;
            }
        }
        evals[idx] = 32768 + white_score - black_score;
    }
    
}
void count_moves_black_pseudokernel(pc* boards, uchar* moves_count, uint n) {
    for (int idx = 0; idx < n; idx++) {
        int board_start_idx = idx * NN1;
        uchar moves = 0;
        for (char x = 0; x < N; x++) {
            for (char y = 0; y < N; y++) {
                switch (boards[board_start_idx + y * N + x])
                {
                case king_black:
                    moves += count_black_king_moves(&boards[board_start_idx], x, y);
                    break;
                case queen_black:
                    moves += count_black_queen_moves(&boards[board_start_idx], x, y);
                    break;
                case rook_black:
                    moves += count_black_rook_moves(&boards[board_start_idx], x, y);
                    break;
                case bishop_black:
                    moves += count_black_bishop_moves(&boards[board_start_idx], x, y);
                    break;
                case knight_black:
                    moves += count_black_knight_moves(&boards[board_start_idx], x, y);
                    break;
                case pawn_black:
                    moves += count_black_pawn_moves(&boards[board_start_idx], x, y);
                    break;
                default:
                    break;
                }
            }
        }
        moves_count[idx] = moves;
    }
}

void count_moves_white_pseudokernel(pc* boards, uchar* moves_count, uint n) {
    for (int idx = 0; idx < n; idx++) {
        int board_start_idx = idx * NN1;
        uchar moves = 0;
        for (char x = 0; x < N; x++) {
            for (char y = 0; y < N; y++) {
                switch (boards[board_start_idx + y * N + x])
                {
                case king_white:
                    moves += count_white_king_moves(&boards[board_start_idx], x, y);
                    break;
                case queen_white:
                    moves += count_white_queen_moves(&boards[board_start_idx], x, y);
                    break;
                case rook_white:
                    moves += count_white_rook_moves(&boards[board_start_idx], x, y);
                    break;
                case bishop_white:
                    moves += count_white_bishop_moves(&boards[board_start_idx], x, y);
                    break;
                case knight_white:
                    moves += count_white_knight_moves(&boards[board_start_idx], x, y);
                    break;
                case pawn_white:
                    moves += count_white_pawn_moves(&boards[board_start_idx], x, y);
                    break;
                default:
                    break;
                }
            }
        }
        moves_count[idx] = moves;
    }
}

void transfer_moves_to_sum_pseudokernel(uchar* moves_count, uint* moves_sum, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        moves_sum[idx] = (uint)moves_count[idx];
    }
}

void transfer_comparisons_to_sum_pseudokernel(bool* comparisons, uint* comparisons_sum, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        comparisons_sum[idx] = (uint)comparisons[idx];
    }
}

void sum_moves_pseudokernel(uint* moves_sum, uint n) {
    uint moves = 0;
    for (uint idx = 0; idx < n; idx++) {
        moves += moves_sum[idx];
    }
    moves_sum[0] = moves;
}

void sum_comparisons_pseudokernel(uint* comparisons, uint n) {
    uint moves = 0;
    for (uint idx = 0; idx < n; idx++) {
        moves += comparisons[idx];
    }
    comparisons[0] = moves;
}



void prescan_pseudokernel(uint* scan_results, uchar* moves_count, uint n) {
    scan_results[0] = 0;
    for (uint idx = 1; idx < n; idx++) {
        scan_results[idx] = scan_results[idx - 1] + (uint)moves_count[idx - 1];
    }
}

void prescan_pseudokernel(uint* scan_results, bool* comparisons, uint n) {
    scan_results[0] = 0;
    for (uint idx = 1; idx < n; idx++) {
        scan_results[idx] = scan_results[idx - 1] + (uint)comparisons[idx - 1];
    }
}

void make_all_black_moves_pseudokernel(pc* dNew_boards, pc* dBoards, uint* scan_results, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        make_all_black_moves_for_board(&(dNew_boards[scan_results[idx] * NN1]), &(dBoards[idx * NN1]));
    }
}

void make_all_black_moves_pseudokernel(pc* dNew_boards, pc* dBoards, uint* scan_results, uchar* saved_moves, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        make_all_black_moves_for_board(&(dNew_boards[scan_results[idx] * NN1]), &(dBoards[idx * NN1]), &(saved_moves[scan_results[idx] *4]));
    }
}

void make_all_white_moves_pseudokernel(pc* dNew_boards, pc* dBoards, uint* scan_results, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        make_all_white_moves_for_board(&(dNew_boards[scan_results[idx] * NN1]), &(dBoards[idx * NN1]));
    }
}

void extend_evaluations_pseudokernel(ushort* dEvals_white_extended, ushort* dEvals_white, uchar* dMoves_count, uint* scan_results, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        for (uchar i = 0; i < dMoves_count[idx]; i++) {
            dEvals_white_extended[scan_results[idx] + i] = dEvals_white[idx];
        }
    }
}

void compare_black_pseudokernel(ushort* dEvals_black, ushort* dEvals_white, bool* dEvals_results, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        dEvals_results[idx] = dEvals_black[idx] <= dEvals_white[idx];
    }
}

void compare_white_pseudokernel(ushort* dEvals_white, ushort* dEvals_black, bool* dEvals_results, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        dEvals_results[idx] = dEvals_white[idx] >= dEvals_black[idx];
    }
}

void copy_selected_boards_pseudokernel(pc* new_dBoards, ushort* new_dEvals, pc* dNew_boards, ushort* dEvals, bool* dEvals_comparison_results, uint* scan_results, uint n) {
    for (uint idx = 0; idx < n; idx++) {
        if (dEvals_comparison_results[idx] == true) {
            new_dEvals[scan_results[idx]] = dEvals[idx];
            for (uchar i = 0; i < NN1; i++) {
                new_dBoards[NN1*scan_results[idx] + i] = dNew_boards[NN1*idx+i];
            }
        }
    }
}

void find_minimum_scoring_moves_pseudokernel(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint n, uchar* moves, ushort* score, uint* i) {
    for (int idx = 0; idx < n; idx++) {
        if (dEvals_black[idx] < *score) {
            *score = dEvals_black[idx];
            *i = dBoards[NN1*idx + NN];
        }
    }
}

void set_comparisons_to_one_pseudokernel(bool* dEvals, int n) {
    for (int idx = 0; idx < n; idx++) {
        dEvals[idx] = 1;
    }
}