#pragma once
#include "consts_and_imports.cuh"


__device__ __host__ void make_new_board_from_move(pc* new_board, pc* board, uchar x, uchar y, uchar x_new, uchar y_new, uint* i) {
    for (uchar idx = 0; idx < NN1; idx++) {
        new_board[idx] = board[idx];
    }
    
    new_board[y_new * N + x_new] = new_board[y * N + x];
    new_board[y * N + x] = 0;
    *i = *i + 1;
}

__device__ __host__ void make_new_board_from_move(pc* new_board, pc* board, uchar x, uchar y, uchar x_new, uchar y_new, uint* i, uchar* saved_moves) {
    for (uchar idx = 0; idx < NN1; idx++) {
        new_board[idx] = board[idx];
    }
    new_board[NN] = *i;

    new_board[y_new * N + x_new] = new_board[y * N + x];
    new_board[y * N + x] = 0;
    *i = *i + 1;
    saved_moves[0] = x;
    saved_moves[1] = y;
    saved_moves[2] = x_new;
    saved_moves[3] = y_new;


}
/// MOVES COUNTING
//ok
__device__ __host__ uchar count_black_king_moves(pc* board, char x, char y) {
    uchar moves = 0;
    if (x - 1 >= 0 && board[y * N + x - 1] < pawn_black) moves++;
    if (x + 1 < N && board[y * N + x + 1] < pawn_black) moves++;
    if (y - 1 >= 0 && board[(y - 1) * N + x] < pawn_black) moves++;
    if (y + 1 < N && board[(y + 1) * N + x] < pawn_black) moves++;

    if (x - 1 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 1] < pawn_black) moves++;
    if (x + 1 < N && y - 1 >= 0 && board[(y - 1) * N + x + 1] < pawn_black) moves++;
    if (x - 1 >= 0 && y + 1 < N && board[(y + 1) * N + x - 1] < pawn_black) moves++;
    if (x + 1 < N && y + 1 < N && board[(y + 1) * N + x + 1] < pawn_black) moves++;
    return moves;
}

//ok
__device__ __host__ uchar count_black_queen_moves(pc* board, char x, char y) {
    uchar moves = 0;

    char x_temp;
    char y_temp;

    // bishop-like
    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //rook-like
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    return moves;
}
//ok
__device__ __host__ uchar count_black_rook_moves(pc* board, char x, char y) {
    uchar moves = 0;

    char x_temp;
    char y_temp;
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    return moves;
}

//ok
__device__ __host__ uchar count_black_bishop_moves(pc* board, char x, char y) {
    uchar moves = 0;

    char x_temp;
    char y_temp;

    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] < pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    return moves;
}

//ok
__device__ __host__ uchar count_black_knight_moves(pc* board, char x, char y) {
    uchar moves = 0;

    if (x - 1 >= 0 && y - 2 >= 0 && board[(y - 2) * N + x - 1] < pawn_black) moves++;
    if (x + 1 < N && y - 2 >= 0 && board[(y - 2) * N + x + 1] < pawn_black) moves++;
    if (x - 1 >= 0 && y + 2 < N && board[(y + 2) * N + x - 1] < pawn_black) moves++;
    if (x + 1 < N && y + 2 < N && board[(y + 2) * N + x + 1] < pawn_black) moves++;

    if (x - 2 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 2] < pawn_black) moves++;
    if (x + 2 < N && y - 1 >= 0 && board[(y - 1) * N + x + 2] < pawn_black) moves++;
    if (x - 2 >= 0 && y + 1 < N && board[(y + 1) * N + x - 2] < pawn_black) moves++;
    if (x + 2 < N && y + 1 < N && board[(y + 1) * N + x + 2] < pawn_black) moves++;

    return moves;
}

//ok
__device__ __host__ uchar count_black_pawn_moves(pc* board, char x, char y) {
    uchar moves = 0;
    //first jump
    if (y == 1) {
        if (board[(y+1) * N + x] == 0 && board[(y+2) * N + x] == 0) moves++;
    }
    //move forward
    if (y + 1 < N && board[(y + 1) * N + x] == 0) moves++;

    //attacks
    if (x - 1 >= 0 && y + 1 < N && board[(y + 1) * N + x - 1] != 0 && board[(y + 1) * N + x - 1] < pawn_black) moves++;
    if (x + 1 < N && y + 1 < N && board[(y + 1) * N + x + 1] != 0 && board[(y + 1) * N + x + 1] < pawn_black) moves++;

    return moves;
}

//ok
__device__ __host__ uchar count_white_king_moves(pc* board, char x, char y) {
    uchar moves = 0;
    if (x - 1 >= 0 && (board[y * N + x - 1] >= pawn_black || board[y * N + x - 1] == 0)) moves++;
    if (x + 1 < N && (board[y * N + x + 1] >= pawn_black || board[y * N + x + 1] == 0)) moves++;
    if (y - 1 >= 0 && (board[(y - 1) * N + x] >= pawn_black || board[(y - 1) * N + x] == 0)) moves++;
    if (y + 1 < N && (board[(y + 1) * N + x] >= pawn_black || board[(y + 1) * N + x] == 0)) moves++;

    if (x - 1 >= 0 && y - 1 >= 0 && (board[(y - 1) * N + x - 1] >= pawn_black || board[(y - 1) * N + x - 1] == 0)) moves++;
    if (x + 1 < N && y - 1 >= 0 && (board[(y - 1) * N + x + 1] >= pawn_black || board[(y - 1) * N + x + 1] == 0)) moves++;
    if (x - 1 >= 0 && y + 1 < N && (board[(y + 1) * N + x - 1] >= pawn_black || board[(y + 1) * N + x - 1] == 0)) moves++;
    if (x + 1 < N && y + 1 < N && (board[(y + 1) * N + x + 1] >= pawn_black || board[(y + 1) * N + x + 1] == 0)) moves++;
    return moves;
}

//ok
__device__ __host__ uchar count_white_queen_moves(pc* board, char x, char y) {
    uchar moves = 0;

    char x_temp;
    char y_temp;

    // bishop-like
    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //rook-like
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    return moves;
}
//ok
__device__ __host__ uchar count_white_rook_moves(pc* board, char x, char y) {
    uchar moves = 0;

    char x_temp;
    char y_temp;
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    return moves;
}
//ok
__device__ __host__ uchar count_white_bishop_moves(pc* board, char x, char y) {
    uchar moves = 0;

    char x_temp;
    char y_temp;

    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) moves++;
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            moves++;
            break;
        }
        else break;
    }
    return moves;
}
//ok
__device__ __host__ uchar count_white_knight_moves(pc* board, char x, char y) {
    uchar moves = 0;

    if (x - 1 >= 0 && y - 2 >= 0 && (board[(y - 2) * N + x - 1] >= pawn_black || board[(y - 2) * N + x - 1] == 0)) moves++;
    if (x + 1 < N && y - 2 >= 0 && (board[(y - 2) * N + x + 1] >= pawn_black || board[(y - 2) * N + x + 1] == 0)) moves++;
    if (x - 1 >= 0 && y + 2 < N && (board[(y + 2) * N + x - 1] >= pawn_black || board[(y + 2) * N + x - 1] == 0)) moves++;
    if (x + 1 < N && y + 2 < N && (board[(y + 2) * N + x + 1] >= pawn_black || board[(y + 2) * N + x + 1] == 0)) moves++;

    if (x - 2 >= 0 && y - 1 >= 0 && (board[(y - 1) * N + x - 2] >= pawn_black || board[(y - 1) * N + x - 2] == 0)) moves++;
    if (x + 2 < N && y - 1 >= 0 && (board[(y - 1) * N + x + 2] >= pawn_black || board[(y - 1) * N + x + 2] == 0)) moves++;
    if (x - 2 >= 0 && y + 1 < N && (board[(y + 1) * N + x - 2] >= pawn_black || board[(y + 1) * N + x - 2] == 0)) moves++;
    if (x + 2 < N && y + 1 < N && (board[(y + 1) * N + x + 2] >= pawn_black || board[(y + 1) * N + x + 2] == 0)) moves++;

    return moves;
}
//ok
__device__ __host__ uchar count_white_pawn_moves(pc* board, char x, char y) {
    uchar moves = 0;
    //first jump
    if (y == N - 2) {
        if (board[(y - 1) * N + x] == 0 && board[(y - 2) * N + x] == 0) moves++;
    }
    //move forward
    if (y - 1 >= 0 && board[(y - 1) * N + x] == 0) moves++;

    //attacks
    if (x - 1 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 1] >= pawn_black) moves++;
    if (x + 1 < N && y - 1 >= 0 && board[(y - 1) * N + x + 1] >= pawn_black) moves++;

    return moves;
}

/// MOVES MAKING
//ok
__device__ __host__ void make_black_king_moves(pc* new_boards, pc* board, char x, char y, uint* i) {
    if (x - 1 >= 0 && board[y * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y, i);
    if (x + 1 < N && board[y * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y, i);
    if (y - 1 >= 0 && board[(y - 1) * N + x] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y-1, i);
    if (y + 1 < N && board[(y + 1) * N + x] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y+1, i);

    if (x - 1 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y-1, i);
    if (x + 1 < N && y - 1 >= 0 && board[(y - 1) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y-1, i);
    if (x - 1 >= 0 && y + 1 < N && board[(y + 1) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y+1, i);
    if (x + 1 < N && y + 1 < N && board[(y + 1) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y+1, i);
    
}
//ok
__device__ __host__ void make_black_queen_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    char x_temp;
    char y_temp;

    // bishop-like
    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //rook-like
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    
}
//ok
__device__ __host__ void make_black_rook_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    char x_temp;
    char y_temp;
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y_temp, x, y_temp, i);
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    
}
//ok
__device__ __host__ void make_black_bishop_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    char x_temp;
    char y_temp;

    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    
}
//ok
__device__ __host__ void make_black_knight_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    if (x - 1 >= 0 && y - 2 >= 0 && board[(y - 2) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y-2, i);
    if (x + 1 < N && y - 2 >= 0 && board[(y - 2) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y-2, i);
    if (x - 1 >= 0 && y + 2 < N && board[(y + 2) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y+2, i);
    if (x + 1 < N && y + 2 < N && board[(y + 2) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y+2, i);

    if (x - 2 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-2, y-1, i);
    if (x + 2 < N && y - 1 >= 0 && board[(y - 1) * N + x + 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+2, y-1, i);
    if (x - 2 >= 0 && y + 1 < N && board[(y + 1) * N + x - 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-2, y+1, i);
    if (x + 2 < N && y + 1 < N && board[(y + 1) * N + x + 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+2, y+1, i);

    
}
//ok
__device__ __host__ void make_black_pawn_moves(pc* new_boards, pc* board, char x, char y, uint* i) {
    //first jump
    if (y == 1) {
        if (board[(y+1) * N + x] == 0 && board[(y+2) * N + x] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y+2, i);
    }
    //move forward
    if (y + 1 < N && board[(y + 1) * N + x] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y+1, i);

    //attacks
    if (x - 1 >= 0 && y + 1 < N && board[(y + 1) * N + x - 1] != 0 && board[(y + 1) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y+1, i);
    if (x + 1 < N && y + 1 < N && board[(y + 1) * N + x + 1] != 0 && board[(y + 1) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y+1, i);

    
}

//ok
__device__ __host__ void make_white_king_moves(pc* new_boards, pc* board, char x, char y, uint* i) {
    if (x - 1 >= 0 && (board[y * N + x - 1] >= pawn_black || board[y * N + x - 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y, i);
    if (x + 1 < N && (board[y * N + x + 1] >= pawn_black || board[y * N + x + 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y, i);
    if (y - 1 >= 0 && (board[(y - 1) * N + x] >= pawn_black || board[(y - 1) * N + x] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y-1, i);
    if (y + 1 < N && (board[(y + 1) * N + x] >= pawn_black || board[(y + 1) * N + x] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y+1, i);

    if (x - 1 >= 0 && y - 1 >= 0 && (board[(y - 1) * N + x - 1] >= pawn_black || board[(y - 1) * N + x - 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y-1, i);
    if (x + 1 < N && y - 1 >= 0 && (board[(y - 1) * N + x + 1] >= pawn_black || board[(y - 1) * N + x + 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y-1, i);
    if (x - 1 >= 0 && y + 1 < N && (board[(y + 1) * N + x - 1] >= pawn_black || board[(y + 1) * N + x - 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y+1, i);
    if (x + 1 < N && y + 1 < N && (board[(y + 1) * N + x + 1] >= pawn_black || board[(y + 1) * N + x + 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y+1, i);
    
}

//ok
__device__ __host__ void make_white_queen_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    char x_temp;
    char y_temp;

    // bishop-like
    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //rook-like
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    
}
//ok
__device__ __host__ void make_white_rook_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    char x_temp;
    char y_temp;
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    
}
//ok
__device__ __host__ void make_white_bishop_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    char x_temp;
    char y_temp;

    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
        else if (board[y_temp * N + x_temp] >= pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i);
            break;
        }
        else break;
    }
    
}
//ok
__device__ __host__ void make_white_knight_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    if (x - 1 >= 0 && y - 2 >= 0 && (board[(y - 2) * N + x - 1] >= pawn_black || board[(y - 2) * N + x - 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y-2, i);
    if (x + 1 < N && y - 2 >= 0 && (board[(y - 2) * N + x + 1] >= pawn_black || board[(y - 2) * N + x + 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y-2, i);
    if (x - 1 >= 0 && y + 2 < N && (board[(y + 2) * N + x - 1] >= pawn_black || board[(y + 2) * N + x - 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y+2, i);
    if (x + 1 < N && y + 2 < N && (board[(y + 2) * N + x + 1] >= pawn_black || board[(y + 2) * N + x + 1] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y+2, i);

    if (x - 2 >= 0 && y - 1 >= 0 && (board[(y - 1) * N + x - 2] >= pawn_black || board[(y - 1) * N + x - 2] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-2, y-1, i);
    if (x + 2 < N && y - 1 >= 0 && (board[(y - 1) * N + x + 2] >= pawn_black || board[(y - 1) * N + x + 2] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+2, y-1, i);
    if (x - 2 >= 0 && y + 1 < N && (board[(y + 1) * N + x - 2] >= pawn_black || board[(y + 1) * N + x - 2] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-2, y+1, i);
    if (x + 2 < N && y + 1 < N && (board[(y + 1) * N + x + 2] >= pawn_black || board[(y + 1) * N + x + 2] == 0)) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+2, y+1, i);

    
}
//ok
__device__ __host__ void make_white_pawn_moves(pc* new_boards, pc* board, char x, char y, uint* i) {

    //first jump
    if (y == N - 2) {
        if (board[(y - 1) * N + x] == 0 && board[(y - 2) * N + x] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y-2, i);
    }
    //move forward
    if (y - 1 >= 0 && board[(y - 1) * N + x] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y-1, i);

    //attacks
    if (x - 1 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 1] >= pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x-1, y-1, i);
    if (x + 1 < N && y - 1 >= 0 && board[(y - 1) * N + x + 1] >= pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x+1, y-1, i);
   
}

//MOVES WITH SAVE
//ok
__device__ __host__ void make_black_king_moves(pc* new_boards, pc* board, char x, char y, uint* i, uchar* saved_moves) {
    if (x - 1 >= 0 && board[y * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 1, y, i, &(saved_moves[4* *i]));
    if (x + 1 < N && board[y * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 1, y, i, &(saved_moves[4* *i]));
    if (y - 1 >= 0 && board[(y - 1) * N + x] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y - 1, i, &(saved_moves[4* *i]));
    if (y + 1 < N && board[(y + 1) * N + x] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y + 1, i, &(saved_moves[4* *i]));

    if (x - 1 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 1, y - 1, i, &(saved_moves[4* *i]));
    if (x + 1 < N && y - 1 >= 0 && board[(y - 1) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 1, y - 1, i, &(saved_moves[4* *i]));
    if (x - 1 >= 0 && y + 1 < N && board[(y + 1) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 1, y + 1, i, &(saved_moves[4* *i]));
    if (x + 1 < N && y + 1 < N && board[(y + 1) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 1, y + 1, i, &(saved_moves[4* *i]));

}
//ok
__device__ __host__ void make_black_queen_moves(pc* new_boards, pc* board, char x, char y, uint* i, uchar* saved_moves) {

    char x_temp;
    char y_temp;

    // bishop-like
    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //rook-like
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }

}
//ok
__device__ __host__ void make_black_rook_moves(pc* new_boards, pc* board, char x, char y, uint* i, uchar* saved_moves) {

    char x_temp;
    char y_temp;
    //up
    for (x_temp = x, y_temp = y - 1; y_temp >= 0; y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y_temp, x, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //down
    for (x_temp = x, y_temp = y + 1; y_temp < N; y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //left
    for (x_temp = x - 1, y_temp = y; x_temp >= 0; x_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //right
    for (x_temp = x + 1, y_temp = y; x_temp < N; x_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }

}
//ok
__device__ __host__ void make_black_bishop_moves(pc* new_boards, pc* board, char x, char y, uint* i, uchar* saved_moves) {

    char x_temp;
    char y_temp;

    //up left
    for (x_temp = x - 1, y_temp = y - 1; x_temp >= 0 && y_temp >= 0; x_temp--, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //up right
    for (x_temp = x + 1, y_temp = y - 1; x_temp < N && y_temp >= 0; x_temp++, y_temp--) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //down left
    for (x_temp = x - 1, y_temp = y + 1; x_temp >= 0 && y_temp < N; x_temp--, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }
    //down right
    for (x_temp = x + 1, y_temp = y + 1; x_temp < N && y_temp < N; x_temp++, y_temp++) {
        if (board[y_temp * N + x_temp] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
        else if (board[y_temp * N + x_temp] < pawn_black) {
            make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x_temp, y_temp, i, &(saved_moves[4* *i]));
            break;
        }
        else break;
    }

}
//ok
__device__ __host__ void make_black_knight_moves(pc* new_boards, pc* board, char x, char y, uint* i, uchar* saved_moves) {

    if (x - 1 >= 0 && y - 2 >= 0 && board[(y - 2) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 1, y - 2, i, &(saved_moves[4* *i]));
    if (x + 1 < N && y - 2 >= 0 && board[(y - 2) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 1, y - 2, i, &(saved_moves[4* *i]));
    if (x - 1 >= 0 && y + 2 < N && board[(y + 2) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 1, y + 2, i, &(saved_moves[4* *i]));
    if (x + 1 < N && y + 2 < N && board[(y + 2) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 1, y + 2, i, &(saved_moves[4* *i]));

    if (x - 2 >= 0 && y - 1 >= 0 && board[(y - 1) * N + x - 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 2, y - 1, i, &(saved_moves[4* *i]));
    if (x + 2 < N && y - 1 >= 0 && board[(y - 1) * N + x + 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 2, y - 1, i, &(saved_moves[4* *i]));
    if (x - 2 >= 0 && y + 1 < N && board[(y + 1) * N + x - 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 2, y + 1, i, &(saved_moves[4* *i]));
    if (x + 2 < N && y + 1 < N && board[(y + 1) * N + x + 2] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 2, y + 1, i, &(saved_moves[4* *i]));


}
//ok
__device__ __host__ void make_black_pawn_moves(pc* new_boards, pc* board, char x, char y, uint* i, uchar* saved_moves) {
    //first jump
    if (y == 1) {
        if (board[(y + 1) * N + x] == 0 && board[(y + 2) * N + x] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y + 2, i, &(saved_moves[4* *i]));
    }
    //move forward
    if (y + 1 < N && board[(y + 1) * N + x] == 0) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x, y + 1, i, &(saved_moves[4* *i]));

    //attacks
    if (x - 1 >= 0 && y + 1 < N && board[(y + 1) * N + x - 1] != 0 && board[(y + 1) * N + x - 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x - 1, y + 1, i, &(saved_moves[4* *i]));
    if (x + 1 < N && y + 1 < N && board[(y + 1) * N + x + 1] != 0 && board[(y + 1) * N + x + 1] < pawn_black) make_new_board_from_move(&(new_boards[*i * NN1]), board, x, y, x + 1, y + 1, i, &(saved_moves[4* *i]));


}


__device__ __host__ void make_all_black_moves_for_board(pc* new_boards, pc* board, uchar* saved_moves) {
    uint i = 0;
    for (char x = 0; x < N; x++) {
        for (char y = 0; y < N; y++) {
            switch (board[y * N + x]) {
            case king_black:
                make_black_king_moves(new_boards, board, x, y, &i, saved_moves);
                break;
            case queen_black:
                make_black_queen_moves(new_boards, board, x, y, &i, saved_moves);
                break;
            case rook_black:
                make_black_rook_moves(new_boards, board, x, y, &i, saved_moves);
                break;
            case bishop_black:
                make_black_bishop_moves(new_boards, board, x, y, &i, saved_moves);
                break;
            case knight_black:
                make_black_knight_moves(new_boards, board, x, y, &i, saved_moves);
                break;
            case pawn_black:
                make_black_pawn_moves(new_boards, board, x, y, &i, saved_moves);
                break;
            default:
                break;
            }
        }
    }
}

__device__ __host__ void make_all_black_moves_for_board(pc* new_boards, pc* board) {
    uint i = 0;
    for (char x = 0; x < N; x++) {
        for (char y = 0; y < N; y++) {
            switch (board[y*N + x]) {
            case king_black:
                make_black_king_moves(new_boards, board, x, y, &i);
                break;
            case queen_black:
                make_black_queen_moves(new_boards, board, x, y, &i);
                break;
            case rook_black:
                make_black_rook_moves(new_boards, board, x, y, &i);
                break;
            case bishop_black:
                make_black_bishop_moves(new_boards, board, x, y, &i);
                break;
            case knight_black:
                make_black_knight_moves(new_boards, board, x, y, &i);
                break;
            case pawn_black:
                make_black_pawn_moves(new_boards, board, x, y, &i);
                break;
            default:
                break;
            }
        }
    }
}


__device__ __host__ void make_all_white_moves_for_board(pc* new_boards, pc* board) {
    uint i = 0;
    for (char x = 0; x < N; x++) {
        for (char y = 0; y < N; y++) {
            switch (board[y * N + x]) {
            case king_white:
                make_white_king_moves(new_boards, board, x, y, &i);
                break;
            case queen_white:
                make_white_queen_moves(new_boards, board, x, y, &i);
                break;
            case rook_white:
                make_white_rook_moves(new_boards, board, x, y, &i);
                break;
            case bishop_white:
                make_white_bishop_moves(new_boards, board, x, y, &i);
                break;
            case knight_white:
                make_white_knight_moves(new_boards, board, x, y, &i);
                break;
            case pawn_white:
                make_white_pawn_moves(new_boards, board, x, y, &i);
                break;
            default:
                break;
            }
        }
    }

}