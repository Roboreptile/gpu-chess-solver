#pragma once
#include "cpu_gpu_funcs.cuh"

__device__ ushort atomicMin(ushort* address, ushort val) {
    int* address_as_i = (int*)address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            ::fminf(val, assumed));
    } while (assumed != old);
    return (ushort)old;
}

__global__ void evaluate_kernel(pc* boards, ushort* evals, uint n) {

    uint idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    uint board_start_idx = idx * NN1;

    ushort white_score = 0, black_score = 0;
    for (uint i = 0; i < NN; i++) {
        switch (boards[board_start_idx + i]) {
        case king_white:
            white_score += K;
            white_score += dK_scores_w[i];
            break;
        case queen_white:
            white_score += Q;
            white_score += dQ_scores_w[i];
            break;
        case rook_white:
            white_score += R;
            white_score += dR_scores_w[i];
            break;
        case bishop_white:
            white_score += B;
            white_score += dB_scores_w[i];
            break;
        case knight_white:
            white_score += Kn;
            white_score += dK_scores_w[i];
            break;
        case pawn_white:
            white_score += P;
            white_score += dP_scores_w[i];
            break;
        case king_black:
            black_score += K;
            black_score += dK_scores_b[i];
            break;
        case queen_black:
            black_score += Q;
            black_score += dQ_scores_b[i];
            break;
        case rook_black:
            black_score += R;
            black_score += dR_scores_b[i];
            break;
        case bishop_black:
            black_score += B;
            black_score += dB_scores_b[i];
            break;
        case knight_black:
            black_score += N;
            black_score += dN_scores_b[i];
            break;
        case pawn_black:
            black_score += P;
            black_score += dP_scores_b[i];
            break;
        default:
            break;
        }
    }
    
    evals[idx] = 32768 + white_score - black_score;
};

__global__ void count_moves_black_kernel(pc* boards, uchar* moves_count, uint n) {
    uint idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    uint board_start_idx = idx * NN1;
    uchar moves = 0;
    for(char x = 0; x<N; x++){
        for (char y = 0; y < N; y++) {
            switch (boards[board_start_idx + y*N + x])
            {
            case king_black:
                moves += count_black_king_moves(&boards[board_start_idx], x, y);
                break;
            case queen_black:
                moves += count_black_queen_moves(&boards[board_start_idx], x, y);
                break;
            case rook_black:
                moves += count_black_rook_moves(&boards[board_start_idx], x, y);
                break;
            case bishop_black:
                moves += count_black_bishop_moves(&boards[board_start_idx], x, y);
                break;
            case knight_black:
                moves += count_black_knight_moves(&boards[board_start_idx], x, y);
                break;
            case pawn_black:
                moves += count_black_pawn_moves(&boards[board_start_idx], x, y);
                break;
            default:
                break;
            }
        }
    }
    moves_count[idx] = (uchar)moves;
}


__global__ void count_moves_white_kernel(pc* boards, uchar* moves_count, uint n) {
    uint idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    uint board_start_idx = idx * NN1;
    uchar moves = 0;
    for (char x = 0; x < N; x++) {
        for (char y = 0; y < N; y++) {
            switch (boards[board_start_idx + y * N + x])
            {
            case king_white:
                moves += count_white_king_moves(&boards[board_start_idx], x, y);
                break;
            case queen_white:
                moves += count_white_queen_moves(&boards[board_start_idx], x, y);
                break;
            case rook_white:
                moves += count_white_rook_moves(&boards[board_start_idx], x, y);
                break;
            case bishop_white:
                moves += count_white_bishop_moves(&boards[board_start_idx], x, y);
                break;
            case knight_white:
                moves += count_white_knight_moves(&boards[board_start_idx], x, y);
                break;
            case pawn_white:
                moves += count_white_pawn_moves(&boards[board_start_idx], x, y);
                break;
            default:
                break;
            }
        }
    }
    moves_count[idx] = (uint)moves;
}

__global__ void transfer_moves_to_sum_kernel(uchar* moves_count, uint* moves_sum, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    moves_sum[idx] = (uint)moves_count[idx];
}

__global__ void transfer_comparisons_to_sum_kernel(bool* comparisons, uint* comparisons_sum, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    comparisons_sum[idx] = (uint)comparisons[idx];
}

__global__ void sumReduceEven(uint* moves_count, uint n) {
    int idx = blockIdx.x*TPB + threadIdx.x;
    if (idx >= n) return;
    moves_count[idx] += moves_count[idx + n];
}

__global__ void sumReduceOdd(uint* moves_count, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    moves_count[idx + 1] += moves_count[idx + n + 1];
}

__global__ void prescan_kernel(uint* scan_results, uchar* moves_count, uint n) {
    scan_results[0] = 0;
    for (uint idx = 1; idx < n; idx++) {
        scan_results[idx] = scan_results[idx - 1] + (uint)moves_count[idx - 1];
    }
}

__global__ void prescan_kernel(uint* scan_results, bool* comparisons, uint n) {
    scan_results[0] = 0;
    for (uint idx = 1; idx < n; idx++) {
        scan_results[idx] = scan_results[idx - 1] + (uint)comparisons[idx - 1];
    }
}

__global__ void prescan_kernel_v2(uint* scan_results, uchar* moves_count, uint n) {
    extern  __shared__  uint temp[];

    int thid = threadIdx.x;
    int bid = blockIdx.x;


    int offset = 1;
    if ((bid * TPB + thid) < n) {
        temp[thid] = (uint)moves_count[bid * TPB + thid];
    }
    else {
        temp[thid] = 0;
    }

    for (int d = TPB >> 1; d > 0; d >>= 1)
    {
        __syncthreads();
        if (thid < d)
        {
            int ai = offset * (2 * thid + 1) - 1;
            int bi = offset * (2 * thid + 2) - 1;
            temp[bi] += temp[ai];
        }
        offset *= 2;
    }

    if (thid == 0) temp[TPB - 1] = 0;

    for (int d = 1; d < TPB; d *= 2)
    {
        offset >>= 1;
        __syncthreads();
        if (thid < d)
        {
            int ai = offset * (2 * thid + 1) - 1;
            int bi = offset * (2 * thid + 2) - 1;
            float t = temp[ai];
            temp[ai] = temp[bi];
            temp[bi] += t;
        }
    }
    __syncthreads();

    scan_results[bid * TPB + thid] = temp[thid];
}

__global__ void prescan_kernel_v2(uint* scan_results, bool* comparisons, uint n) {
    extern  __shared__  uint temp[];

    int thid = threadIdx.x;
    int bid = blockIdx.x;

    int offset = 1;
    if ((bid * TPB + thid) < n) {
        temp[thid] = (uint)comparisons[bid * TPB + thid];
    }
    else {
        temp[thid] = 0;
    }

    for (int d = TPB >> 1; d > 0; d >>= 1)
    {
        __syncthreads();
        if (thid < d)
        {
            int ai = offset * (2 * thid + 1) - 1;
            int bi = offset * (2 * thid + 2) - 1;
            temp[bi] += temp[ai];
        }
        offset *= 2;
    }

    if (thid == 0) temp[TPB - 1] = 0;

    for (int d = 1; d < TPB; d *= 2)

    {
        offset >>= 1;
        __syncthreads();
        if (thid < d)
        {
            int ai = offset * (2 * thid + 1) - 1;
            int bi = offset * (2 * thid + 2) - 1;
            float t = temp[ai];
            temp[ai] = temp[bi];
            temp[bi] += t;
        }
    }
    __syncthreads();

    scan_results[bid * TPB + thid] = temp[thid];
}


__global__ void make_all_black_moves_kernel(pc* dNew_boards, pc* dBoards, uint* scan_results, uchar* saved_moves, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    make_all_black_moves_for_board(&(dNew_boards[scan_results[idx] * NN1]), &(dBoards[idx * NN1]), &(saved_moves[scan_results[idx] * 4]));
}

__global__ void make_all_black_moves_kernel(pc* dNew_boards, pc* dBoards, uint* scan_results, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    make_all_black_moves_for_board(&(dNew_boards[scan_results[idx] * NN1]), &(dBoards[idx * NN1]));
}

__global__ void make_all_white_moves_kernel(pc* dNew_boards, pc* dBoards, uint* scan_results, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    make_all_white_moves_for_board(&(dNew_boards[scan_results[idx] * NN1]), &(dBoards[idx * NN1]));
}

__global__ void extend_evaluations_kernel(ushort* dEvals_white_extended, ushort* dEvals_white, uchar* dMoves_count, uint* scan_results, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    for (uchar i = 0; i < dMoves_count[idx]; i++) {
        dEvals_white_extended[scan_results[idx] + i] = dEvals_white[idx];
    }
}

__global__ void compare_black_kernel(ushort* dEvals_black, ushort* dEvals_white, bool* dEvals_results, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    dEvals_results[idx] = dEvals_black[idx] <= dEvals_white[idx];
}

__global__ void compare_white_kernel(ushort* dEvals_white, ushort* dEvals_black, bool* dEvals_results, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    dEvals_results[idx] = dEvals_white[idx] >= dEvals_black[idx];
}

__global__ void copy_selected_boards_kernel(pc* new_dBoards, ushort* new_dEvals, pc* dNew_boards, ushort* dEvals, bool* dEvals_comparison_results, uint* scan_results, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n || !dEvals_comparison_results[idx]) return;

    new_dEvals[scan_results[idx]] = dEvals[idx];
    for (uchar i = 0; i < NN1; i++) {
        new_dBoards[NN1 * scan_results[idx] + i] = dNew_boards[NN1 * idx + i];
    }
}

__global__ void find_minimum_score_kernel(ushort* dEvals_black, uint n, int* score) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    int eval = dEvals_black[idx];

    if (eval < *score) {
        atomicMin(score,eval);
    }
}

__global__ void minReduceEven(uint* moves_count, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    if(moves_count[idx + n]<moves_count[idx]) moves_count[idx] += moves_count[idx + n];
}

__global__ void minReduceOdd(uint* moves_count, uint n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    if (moves_count[idx + n + 1] < moves_count[idx + 1]) moves_count[idx + 1] += moves_count[idx + n + 1];
}

__global__ void find_minimum_scoring_move_kernel(pc* dBoards, ushort* dEvals_black, uint n, int* score, uint* i) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;

    ushort eval = dEvals_black[idx];
    ushort score_ushort = *score;

    if (eval == score_ushort) {
        atomicMax(i, (int)dBoards[NN1 * idx + NN]);
    }
}

__global__ void set_comparisons_to_one_kernel(bool* dEvals, int n) {
    int idx = blockIdx.x * TPB + threadIdx.x;
    if (idx >= n) return;
    dEvals[idx] = 1;
}