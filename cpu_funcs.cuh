#pragma once
#include "cpu_pseudokernels.cuh"

using namespace std;

template <typename T, typename Y>
void debug_printCPU(T* arr, Y* count);

void init_constant_weightsCPU() {
    hP_scores_w = new int[NN] {
        0, 0, 0, 0, 0, 0, 0, 0,
            50, 50, 50, 50, 50, 50, 50, 50,
            10, 10, 20, 30, 30, 20, 10, 10,
            5, 5, 10, 25, 25, 10, 5, 5,
            0, 0, 0, 20, 20, 0, 0, 0,
            5, -5, -10, 0, 0, -10, -5, 5,
            5, 10, 10, -20, -20, 10, 10, 5,
            0, 0, 0, 0, 0, 0, 0, 0
    };
    hN_scores_w = new int[NN] {
        -50, -40, -30, -30, -30, -30, -40, -50,
            -40, -20, 0, 0, 0, 0, -20, -40,
            -30, 0, 10, 15, 15, 10, 0, -30,
            -30, 5, 15, 20, 20, 15, 5, -30,
            -30, 0, 15, 20, 20, 15, 0, -30,
            -30, 5, 10, 15, 15, 10, 5, -30,
            -40, -20, 0, 5, 5, 0, -20, -40,
            -50, -40, -30, -30, -30, -30, -40, -50
    };
    hB_scores_w = new int[NN] {
        -20, -10, -10, -10, -10, -10, -10, -20,
            -10, 0, 0, 0, 0, 0, 0, -10,
            -10, 0, 5, 10, 10, 5, 0, -10,
            -10, 5, 5, 10, 10, 5, 5, -10,
            -10, 0, 10, 10, 10, 10, 0, -10,
            -10, 10, 10, 10, 10, 10, 10, -10,
            -10, 5, 0, 0, 0, 0, 5, -10,
            -20, -10, -10, -10, -10, -10, -10, -20
    };
    hR_scores_w = new int[NN] {
        0, 0, 0, 0, 0, 0, 0, 0,
            5, 10, 10, 10, 10, 10, 10, 5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            0, 0, 0, 5, 5, 0, 0, 0
    };
    hQ_scores_w = new int[NN] {
        -20, -10, -10, -5, -5, -10, -10, -20,
            -10, 0, 0, 0, 0, 0, 0, -10,
            -10, 0, 5, 5, 5, 5, 0, -10,
            -5, 0, 5, 5, 5, 5, 0, -5,
            0, 0, 5, 5, 5, 5, 0, -5,
            -10, 5, 5, 5, 5, 5, 0, -10,
            -10, 0, 5, 0, 0, 0, 0, -10,
            -20, -10, -10, -5, -5, -10, -10, -20
    };
    hK_scores_w = new int[NN] {
        -30, -40, -40, -50, -50, -40, -40, -30,
            -30, -40, -40, -50, -50, -40, -40, -30,
            -30, -40, -40, -50, -50, -40, -40, -30,
            -30, -40, -40, -50, -50, -40, -40, -30,
            -20, -30, -30, -40, -40, -30, -30, -20,
            -10, -20, -20, -20, -20, -20, -20, -10,
            20, 20, 0, 0, 0, 0, 20, 20,
            20, 30, 10, 0, 0, 10, 30, 20
    };

    hP_scores_b = new int[NN];
    hN_scores_b = new int[NN];
    hB_scores_b = new int[NN];
    hR_scores_b = new int[NN];
    hQ_scores_b = new int[NN];
    hK_scores_b = new int[NN];


    for (int i = 0; i < NN; i++) {
        int x = i % N;
        int y_b = (i - x) / N;
        int y_w = N - y_b - 1;
        hP_scores_b[x + y_b * N] = hP_scores_w[x + y_w * N];
        hN_scores_b[x + y_b * N] = hN_scores_w[x + y_w * N];
        hB_scores_b[x + y_b * N] = hB_scores_w[x + y_w * N];
        hR_scores_b[x + y_b * N] = hR_scores_w[x + y_w * N];
        hQ_scores_b[x + y_b * N] = hQ_scores_w[x + y_w * N];
        hK_scores_b[x + y_b * N] = hK_scores_w[x + y_w * N];
    }
}


void copy_input_board_to_memoryCPU(pc* input_board, pc** dBoards) {
    dBoards[0] = new pc[NN1];
    for (int i = 0; i < NN; i++) {
        dBoards[0][i] = input_board[i];
    }
}

void evaluateCPU(pc* dBoards, ushort** dEvals, uint* boards_count) {
    *dEvals = new ushort[*boards_count];
    evaluate_pseudokernel(dBoards, *dEvals, *boards_count);
}

void black_count_all_movesCPU(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count) {
    uint* dMoves_sum = new uint[*boards_count];
    *dMoves_count = new uchar[*boards_count];

    count_moves_black_pseudokernel(dBoards, *dMoves_count, *boards_count);
    transfer_moves_to_sum_pseudokernel(*dMoves_count, dMoves_sum, *boards_count);
    sum_moves_pseudokernel(dMoves_sum, *boards_count);
    *new_boards_count = dMoves_sum[0];
    delete[](dMoves_sum);
}

void white_count_all_movesCPU(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count) {
    uint* dMoves_sum = new uint[*boards_count];
    *dMoves_count = new uchar[*boards_count];

    count_moves_white_pseudokernel(dBoards, *dMoves_count, *boards_count);
    transfer_moves_to_sum_pseudokernel(*dMoves_count, dMoves_sum, *boards_count);
    sum_moves_pseudokernel(dMoves_sum, *boards_count);
    *new_boards_count = dMoves_sum[0];
    delete[](dMoves_sum);
}

void allocate_memory_for_new_boardsCPU(pc** dNew_boards, uint* boards_count) {
    *dNew_boards = new pc[NN1 * (*boards_count)];
}

void black_make_all_movesCPU(pc* dNew_boards, pc* dBoards, uchar* moves_count, uint* new_boards_count, uint* boards_count, uchar** dSource_moves, uint** scan_results) {
    *scan_results = new uint[*boards_count];
    *dSource_moves = new uchar[4 * (*new_boards_count)];
    prescan_pseudokernel(*scan_results, moves_count, *boards_count);
    make_all_black_moves_pseudokernel(dNew_boards, dBoards, *scan_results, *dSource_moves, *boards_count);
}


void black_make_all_movesCPU(pc* dNew_boards, pc* dBoards, uchar* moves_count, uint* new_boards_count, uint* boards_count, uint** scan_results) {
    *scan_results = new uint[*boards_count];
    prescan_pseudokernel(*scan_results, moves_count, *boards_count);
    make_all_black_moves_pseudokernel(dNew_boards, dBoards, *scan_results, *boards_count);
}

void white_make_all_movesCPU(pc* dNew_boards, pc* dBoards, uchar* moves_count, uint* new_boards_count, uint* boards_count, uint** scan_results) {
    *scan_results = new uint[*boards_count];
    
    prescan_pseudokernel(*scan_results, moves_count, *boards_count);
    make_all_white_moves_pseudokernel(dNew_boards, dBoards, *scan_results, *boards_count);
}

void black_compareCPU(ushort* dEvals_black, ushort** dEvals_white, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results) {
    ushort* dEvals_white_extended = new ushort[*new_boards_count];
    extend_evaluations_pseudokernel(dEvals_white_extended, *dEvals_white, dMoves_count, dScan_results, *boards_count);
    delete[](*dEvals_white);
    *dEvals_white = dEvals_white_extended;

    *dEvals_comparison_results = new bool[*new_boards_count];
    compare_black_pseudokernel(dEvals_black, *dEvals_white, *dEvals_comparison_results, *new_boards_count);
}

void white_compareCPU(ushort* dEvals_white, ushort** dEvals_black, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results) {
    ushort* dEvals_black_extended = new ushort[*new_boards_count];
    extend_evaluations_pseudokernel(dEvals_black_extended, *dEvals_black, dMoves_count, dScan_results, *boards_count);

    delete[](*dEvals_black);
    *dEvals_black = dEvals_black_extended;

    *dEvals_comparison_results = new bool[*new_boards_count];
    compare_white_pseudokernel(dEvals_white, *dEvals_black, *dEvals_comparison_results, *new_boards_count);
}

void copy_selected_boardsCPU(pc** dBoards, ushort** dEvals, pc* dNew_boards, bool* dEvals_comparison_results, uint* boards_count, uint* new_boards_count) {

    uint* comparisons_sum = new uint[(int)*new_boards_count];
    transfer_comparisons_to_sum_pseudokernel(dEvals_comparison_results, comparisons_sum, *new_boards_count);
    sum_comparisons_pseudokernel(comparisons_sum, *new_boards_count);
    uint new_dBoards_count = comparisons_sum[0];
    delete[](comparisons_sum);

    if (new_dBoards_count == 0) {
        if(debug) cout << "!!!0 - OVERRIDE!!!" << endl;
        new_dBoards_count = *new_boards_count;
        set_comparisons_to_one_pseudokernel(dEvals_comparison_results, *new_boards_count);
    }

    uint* scan_results = new uint[*new_boards_count];
    prescan_pseudokernel(scan_results, dEvals_comparison_results, *new_boards_count);

    pc* new_dBoards = new pc[NN1 * new_dBoards_count];
    ushort* new_dEvals = new ushort[new_dBoards_count];
    copy_selected_boards_pseudokernel(new_dBoards, new_dEvals, dNew_boards, *dEvals, dEvals_comparison_results, scan_results, *new_boards_count);

    delete[](*dBoards);
    delete[](*dEvals);
    *dBoards = new_dBoards;
    *dEvals = new_dEvals;
    *boards_count = new_dBoards_count;

}

void find_move_with_minimum_scoreCPU(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count, uchar* moves) {

    if (*boards_count == 0) {
        printf("ERR: output boards_count is 0, random move will be played\n");
        moves[0] = dSource_moves[4 * 0 + 0];
        moves[1] = dSource_moves[4 * 0 + 1];
        moves[2] = dSource_moves[4 * 0 + 2];
        moves[3] = dSource_moves[4 * 0 + 3];
    }
    else {
        ushort* score = new ushort[1]{ dEvals_black[0] };
        uint* i = new uint[1]{ 0 };
        find_minimum_scoring_moves_pseudokernel(dBoards, dSource_moves, dEvals_black, *boards_count, moves, score, i);
        cout << "SCORE: " << *score << endl;
        moves[0] = dSource_moves[4 * *i + 0];
        moves[1] = dSource_moves[4 * *i + 1];
        moves[2] = dSource_moves[4 * *i + 2];
        moves[3] = dSource_moves[4 * *i + 3];
    }
    
}


void clean_memoryCPU(pc* dNew_boards, uchar* dMoves_count_per_board, uint* dScan_results, bool* dEvals_comparison_results) {
    delete[](dNew_boards);
    delete[](dMoves_count_per_board);
    delete[](dScan_results);
    delete[](dEvals_comparison_results);
}

void clean_memoryCPU(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count) {
    delete[](dBoards);
    delete[](dSource_moves);
    delete[](dEvals_black);
    boards_count[0] = 1;
}

template <typename T, typename Y>
void debug_printCPU(T* arr, Y* count) {
    int c = (int) *count;
    printf("C: %d\n", c);
    for (int i = 0; i < c; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

template <typename T, typename Y>
void debug_printCPU(T* arr, Y* count, int m) {
    int c = (int)*count;
    printf("C: %d\n", c);
    for (int i = 0; i < c; i++) {
        for (int j = 0; j < m; j++) {
            printf("%d ", arr[i*m + j]);
        }
        printf("\n");
    }
    printf("\n");
}

template <typename T, typename Y>
void debug_N_printCPU(T* arr, Y* count) {
    int c = (int) *count;
    printf("C: %d\n", c);
    for (int i = 0; i < c; i++) {
        for (int j = 0; j < NN1; j++) {
            printf("%d ", arr[i*NN1 + j]);
            if ((j + 1) % N == 0) printf("\n");
        }
        printf("\n\n");
    }
}