#include "chess_solver.cuh"

int main() {

    mode = ChessSolverMode::CPU;
    max_depth = 3;
    debug = false;

    pc* input_board = new pc[NN]{               //about 2.2 gigs at the start for depth 3, around 8 gigs for depth 4
        rook_black,knight_black,bishop_black,queen_black,king_black,bishop_black, knight_black, rook_black,
        pawn_black,pawn_black,pawn_black,pawn_black,pawn_black,pawn_black,pawn_black,pawn_black,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        pawn_white,pawn_white,pawn_white,pawn_white,pawn_white,pawn_white,pawn_white,pawn_white,
        rook_white,knight_white,bishop_white,queen_white,king_white,bishop_white, knight_white, rook_white
    };

    pc* input_board_kings_only = new pc[NN]{    // 6 GPU 7 CPU
        0,0,0,0,0,0,0,king_black,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,king_white,
    };
    
    launch_chess_solver(input_board);
    return EXIT_SUCCESS;
}
