#pragma once

#define __CUDACC__

#include "cuda_runtime.h"
#include "device_functions.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <iostream>
#include <chrono>

// Shortcuts for types

#define uint   unsigned int		//4294697296	2^32
#define ushort unsigned short	//65535			2^16 
#define uchar  unsigned char	//255			2^8

// Program settings

#define THREADS_PER_BLOCK 32	//More than 32 seems to cause GPU shared memory to overflow 
#define board_side_len 8
#define piece_encoder uchar

// Program constants

//#define BLACK "\e[0;31m"
//#define WHITE "\e[0;31m"
#define BLACK "B"
#define WHITE "W"

#define pc piece_encoder
#define TPB THREADS_PER_BLOCK

#define N board_side_len
#define NN (N*N)
#define NN1 (NN+1)

// Initial configuration global values

enum ChessSolverMode {
	CPU,
	GPU,
};

ChessSolverMode mode;
int max_depth;
bool debug;

// Weights, Symbols and IDs of pieces

#define pawn_white 1
#define knight_white 2
#define bishop_white 3
#define rook_white 4
#define queen_white 5
#define king_white 6

#define pawn_black 7
#define knight_black 8
#define bishop_black 9
#define rook_black 10
#define queen_black 11
#define king_black 12

#define pawn "i"
#define knight "R"
#define bishop "Y"
#define rook "T"
#define queen "X"
#define king "W"

#define P 100
#define Kn 320
#define B 330
#define R 500
#define Q 900
#define K 20000

// Constant arrays that hold weights

__constant__ int dP_scores_w[NN];
__constant__ int dN_scores_w[NN];
__constant__ int dB_scores_w[NN];
__constant__ int dR_scores_w[NN];
__constant__ int dQ_scores_w[NN];
__constant__ int dK_scores_w[NN];

__constant__ int dP_scores_b[NN];
__constant__ int dN_scores_b[NN];
__constant__ int dB_scores_b[NN];
__constant__ int dR_scores_b[NN];
__constant__ int dQ_scores_b[NN];
__constant__ int dK_scores_b[NN];

int* hP_scores_w, * hN_scores_w, * hB_scores_w, * hR_scores_w, * hQ_scores_w, * hK_scores_w;
int* hP_scores_b, * hN_scores_b, * hB_scores_b, * hR_scores_b, * hQ_scores_b, * hK_scores_b;


	
