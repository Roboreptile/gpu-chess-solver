# Chess Solver GPU

Uses the power of C++ and CUDA to generate the next best chess move!

Uses default chess starting board, however any board of any size is posssible. Modify main.cu to change the starting board, and value of N to change board side size.

## Requirements:

- CUDA - capable device with compute capability 3.0+
- Installed CUDA: [CUDA Installation Guide](https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.htmlhttps://)

## Launch:

1. Navigate to the solution folder, and type the following:
   `nvcc main.cu`
2. A couple of new files will be generated (a.exe, a.lib, a.exp). Type exe name into the console:
   `a.exe`
3. Play chess!

Keep in mind this program uses a lot of memory - for a normal game, requirements for depth 3 is around 2.2 gigabytes, 4 - 7.6 gigabytes, 5 - 26 gigabytes. Please select the proper setting in the main file.

One depth is defined as analyzing every possible black response to every possible white move! This means that the most common online definition of depth

## Solution Description

For great isual reporesentations of every step, turn debug mode to TRUE in the main file.

Pseudo - algorithm description of the solution:

1. User performs a move
2. Take a chess board and input it into GPU memory
3. Evaluate the board(s) -> save the evaluation scores as WHITE  (uses the evaluation function described here: [Simplified Evaulation Function](https://www.chessprogramming.org/Simplified_Evaluation_Function)
4. Generate every possible black move
5. Evaluate the new boards -> save scores as BLACK
6. Compare BLACK and WHITE -> copy only boards for which black is at least equal to white or better
7. Generate every possible white move
8. Evaluate the new boards -> save scores as WHITE
9. Compare WHITE and BLACK -> copy only boards for which white is at least equal to black or better
10. Repeat steps 4 - 9 DEPTH times
11. Find the board with the best BLACK score
12. Perform the move
13. Go back to step 1 until checkmate

## Symbols

- Pawn: i
- Horsy: R
- Rook: T
- Bishop: Y
- Queen: X
- King: W
- Empty space: __

## Results

Starting board. It turns out that the computer loves the Scandinavian gambit!

![](demos/demo.png)

King and pawn endgame:

![](demos/demo2.png)

Time for depth 3 is on average 2 seconds. It can jump towards 7 seconds for compliated positions, and towards endgame reaches around 10 milliseconds.
