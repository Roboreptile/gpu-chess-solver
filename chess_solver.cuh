#pragma once
#include "gpu_funcs.cuh"
#include "cpu_funcs.cuh"
#include "chess_funcs.cuh"

template<typename T, typename Y>
void debug_print(T * arr, Y * count);

template<typename T, typename Y>
void debug_print(T* arr, Y* count, int multiplier);

template<typename T, typename Y>
void debug_N_print(T* arr, Y* count);

void init_constant_weights();
void copy_input_board_to_memory(pc* input_board, pc** dBoards);
void allocate_memory_for_new_boards(pc** dNew_boards, uint* boards_count);

void evaluate(pc* dBoards, ushort** dEvals, uint* boards_count);

void black_count_all_moves(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count);
void white_count_all_moves(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count);

void black_make_all_moves(pc* dNew_boards, pc* dBoards, uint* new_boards_count, uchar* dMoves_count_per_board, uint* boards_count, uchar** dSource_moves, uint** dScan_results);
void black_make_all_moves(pc* dNew_boards, pc* dBoards, uint* boards_count, uchar* dMoves_count_per_board, uint* new_boards_count, uint** dScan_results);
void white_make_all_moves(pc* dNew_boards, pc* dBoards, uint* boards_count, uchar* dMoves_count_per_board, uint* new_boards_count, uint** dScan_results);

void black_compare(ushort* dEvals_black, ushort** dEvals_white, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results);
void white_compare(ushort* dEvals_white, ushort** dEvals_black, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results);

void copy_selected_boards(pc** dBoards, ushort** dEvals_to_filter, pc* dNew_boards, bool* dEvals_comparison_results, uint* boards_count, uint* new_boards_count);

void find_move_with_minimum_score(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count, uchar* output_moves);

void clean_memory(pc* dNew_boards, uchar* dMoves_count_per_board, uint* dScan_results, bool* dEvals_comparison_results);
void clean_memory(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count);

void launch_chess_solver(pc* input_board) {

    // ==== Variables allocation =====
    // CPU
    int current_depth;
    uint* boards_count = new uint[1]{ 1 };
    uint* new_boards_count = new uint[1]{ 1 };
    uchar* output_moves = new uchar[4];
    chrono::steady_clock::time_point start;

    // GPU
    pc* dBoards, * dNew_boards;
    ushort* dEvals_white, * dEvals_black;
    bool* dEvals_comparison_results;
    uchar* dMoves_count_per_board;
    uint* dScan_results;
    uchar* dSource_moves;
    // ==============================

    init_constant_weights();
    user_move(input_board);

    do {
        copy_input_board_to_memory(input_board, &dBoards);

        start = chrono::high_resolution_clock::now();

        evaluate(dBoards, &dEvals_white, boards_count);
        black_count_all_moves(dBoards, &dMoves_count_per_board, boards_count, new_boards_count);
        allocate_memory_for_new_boards(&dNew_boards, new_boards_count);
        // ===============================================================================================================
        black_make_all_moves(dNew_boards, dBoards, new_boards_count, dMoves_count_per_board, boards_count, &dSource_moves, &dScan_results);
        // ===============================================================================================================
        evaluate(dNew_boards, &dEvals_black, new_boards_count);
        black_compare(dEvals_black, &dEvals_white, new_boards_count, boards_count, &dEvals_comparison_results, dMoves_count_per_board, dScan_results);
        copy_selected_boards(&dBoards, &dEvals_black, dNew_boards, dEvals_comparison_results, boards_count, new_boards_count);

        clean_memory(dNew_boards, dMoves_count_per_board, dScan_results, dEvals_comparison_results);

        current_depth = 1;
        while (current_depth<max_depth) {

            print_time_per_depth(start, current_depth);

            white_count_all_moves(dBoards, &dMoves_count_per_board, boards_count, new_boards_count);
            allocate_memory_for_new_boards(&dNew_boards, new_boards_count);
            white_make_all_moves(dNew_boards, dBoards, boards_count, dMoves_count_per_board, new_boards_count, &dScan_results);
            evaluate(dNew_boards, &dEvals_white, new_boards_count);
            white_compare(dEvals_white, &dEvals_black, new_boards_count, boards_count, &dEvals_comparison_results, dMoves_count_per_board, dScan_results);
            copy_selected_boards(&dBoards, &dEvals_white, dNew_boards, dEvals_comparison_results, boards_count, new_boards_count);
            clean_memory(dNew_boards, dMoves_count_per_board, dScan_results, dEvals_comparison_results);

            black_count_all_moves(dBoards, &dMoves_count_per_board, boards_count, new_boards_count);
            allocate_memory_for_new_boards(&dNew_boards, new_boards_count);
            black_make_all_moves(dNew_boards, dBoards, boards_count, dMoves_count_per_board, new_boards_count, &dScan_results);
            evaluate(dNew_boards, &dEvals_black, new_boards_count);
            black_compare(dEvals_black, &dEvals_white, new_boards_count, boards_count, &dEvals_comparison_results, dMoves_count_per_board, dScan_results);
            copy_selected_boards(&dBoards, &dEvals_black, dNew_boards, dEvals_comparison_results, boards_count, new_boards_count);
            clean_memory(dNew_boards, dMoves_count_per_board, dScan_results, dEvals_comparison_results);

            current_depth++;
        }

        print_time_per_depth(start, current_depth);

        find_move_with_minimum_score(dBoards, dSource_moves ,dEvals_black, boards_count, output_moves);
        play_move(input_board, output_moves);
        print_computer_move(output_moves[0], output_moves[1], output_moves[2], output_moves[3]);
        clean_memory(dBoards, dSource_moves, dEvals_black, boards_count);

        if (!no_checkmate(input_board)) break;
        user_move(input_board);

    } while (no_checkmate(input_board));

    print_board(input_board);
    cout << "Checkmate!" << endl;
}


void init_constant_weights() {
    if (mode == ChessSolverMode::CPU) init_constant_weightsCPU();
    else if (mode == ChessSolverMode::GPU) init_constant_weightsGPU();
    else null_mode_exit();
}

void copy_input_board_to_memory(pc* input_board, pc** dBoards) {
    if (mode == ChessSolverMode::CPU) copy_input_board_to_memoryCPU(input_board, dBoards);
    else if (mode == ChessSolverMode::GPU) copy_input_board_to_memoryGPU(input_board, dBoards);
    else null_mode_exit();

    if (debug) {
        cout << "CPY IN" << endl;
        debug_N_print<pc, int>(*dBoards, new int[1]{ 1 });
    }
}

void allocate_memory_for_new_boards(pc** dNew_boards, uint* boards_count) {
    if (mode == ChessSolverMode::CPU) allocate_memory_for_new_boardsCPU(dNew_boards, boards_count);
    else if (mode == ChessSolverMode::GPU) allocate_memory_for_new_boardsGPU(dNew_boards, boards_count);
    else null_mode_exit();
    if (debug) {
        cout << "ALLOCX" << endl;
        //debug_N_print<pc, uint>(*dNew_boards, boards_count);
    }
}

void evaluate(pc* dBoards, ushort** dEvals, uint* boards_count) {
    if (mode == ChessSolverMode::CPU) evaluateCPU(dBoards, dEvals, boards_count);
    else if (mode == ChessSolverMode::GPU) evaluateGPU(dBoards, dEvals, boards_count);
    else null_mode_exit();
    if (debug) {
        cout<<"EVAL" << endl;
        debug_print<ushort,uint>(*dEvals, boards_count);
    }
}

void black_count_all_moves(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count) {
    if (mode == ChessSolverMode::CPU) black_count_all_movesCPU(dBoards, dMoves_count, boards_count, new_boards_count);
    else if (mode == ChessSolverMode::GPU) black_count_all_movesGPU(dBoards, dMoves_count, boards_count, new_boards_count);
    else null_mode_exit();
    if (debug) {
        cout << "B-COUNT" << endl;
        debug_print<uchar, uint>(*dMoves_count, boards_count);
    }
}

void white_count_all_moves(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count) {
    if (mode == ChessSolverMode::CPU) white_count_all_movesCPU(dBoards, dMoves_count, boards_count, new_boards_count);
    else if (mode == ChessSolverMode::GPU) white_count_all_movesGPU(dBoards, dMoves_count, boards_count, new_boards_count);
    else null_mode_exit();
    if (debug) {
        cout << "W-COUNT" << endl;
        debug_print<uchar, uint>(*dMoves_count, boards_count);
    }
}


void black_make_all_moves(pc* dNew_boards, pc* dBoards, uint* new_boards_count, uchar* dMoves_count_per_board, uint* boards_count, uchar** dSource_moves, uint** dScan_results) {
    if (mode == ChessSolverMode::CPU) black_make_all_movesCPU(dNew_boards, dBoards, dMoves_count_per_board, new_boards_count, boards_count, dSource_moves, dScan_results);
    else if (mode == ChessSolverMode::GPU) black_make_all_movesGPU(dNew_boards, dBoards, dMoves_count_per_board, new_boards_count, boards_count, dSource_moves, dScan_results);
    else null_mode_exit();
    if (debug) {
        cout << "B-ALL MOVES SAVE" << endl;
        debug_print<uchar, uint>(dMoves_count_per_board, boards_count);
        debug_print<uchar, uint>(*dSource_moves, new_boards_count, 4);
        debug_N_print<pc, uint>(dNew_boards, new_boards_count);
    }
}

void black_make_all_moves(pc* dNew_boards, pc* dBoards, uint* boards_count, uchar* dMoves_count_per_board, uint* new_boards_count, uint** dScan_results) {
    if (mode == ChessSolverMode::CPU) black_make_all_movesCPU(dNew_boards, dBoards, dMoves_count_per_board, new_boards_count, boards_count, dScan_results);
    else if (mode == ChessSolverMode::GPU) black_make_all_movesGPU(dNew_boards, dBoards, dMoves_count_per_board, new_boards_count, boards_count, dScan_results);
    else null_mode_exit();
    if (debug) {
        cout << "B-ALL MOVES" << endl;
        debug_print<uchar, uint>(dMoves_count_per_board, boards_count);
        debug_N_print<pc, uint>(dNew_boards, new_boards_count);
    }

}

void white_make_all_moves(pc* dNew_boards, pc* dBoards, uint* boards_count, uchar* dMoves_count_per_board, uint* new_boards_count, uint** dScan_results) {
    if (mode == ChessSolverMode::CPU) white_make_all_movesCPU(dNew_boards, dBoards, dMoves_count_per_board, new_boards_count, boards_count, dScan_results);
    else if (mode == ChessSolverMode::GPU) white_make_all_movesGPU(dNew_boards, dBoards, dMoves_count_per_board, new_boards_count, boards_count, dScan_results);
    else null_mode_exit();
    if (debug) {
        cout << "W-ALL MOVES" << endl;
        debug_print<uchar, uint>(dMoves_count_per_board, boards_count);
        debug_N_print<pc, uint>(dNew_boards, new_boards_count);
    }
}

void black_compare(ushort* dEvals_black, ushort** dEvals_white, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results) {
    if (mode == ChessSolverMode::CPU) black_compareCPU(dEvals_black, dEvals_white, new_boards_count, boards_count, dEvals_comparison_results, dMoves_count, dScan_results);
    else if (mode == ChessSolverMode::GPU) black_compareGPU(dEvals_black, dEvals_white, new_boards_count, boards_count, dEvals_comparison_results, dMoves_count, dScan_results);
    else null_mode_exit();
    if (debug) {
        cout << "B-CMP" << endl;
        debug_print<bool, uint>(*dEvals_comparison_results, new_boards_count);
    }
}

void white_compare(ushort* dEvals_white, ushort** dEvals_black, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results) {
    if (mode == ChessSolverMode::CPU) white_compareCPU(dEvals_white, dEvals_black, new_boards_count, boards_count, dEvals_comparison_results, dMoves_count, dScan_results);
    else if (mode == ChessSolverMode::GPU) white_compareGPU(dEvals_white, dEvals_black, new_boards_count, boards_count, dEvals_comparison_results, dMoves_count, dScan_results);
    else null_mode_exit();
    if (debug) {
        cout << "W-CMP" << endl;
        debug_print<bool, uint>(*dEvals_comparison_results, new_boards_count);
    }
}

void copy_selected_boards(pc** dBoards, ushort** dEvals_to_filter, pc* dNew_boards, bool* dEvals_comparison_results, uint* boards_count, uint* new_boards_count) {
    if (mode == ChessSolverMode::CPU) copy_selected_boardsCPU(dBoards, dEvals_to_filter, dNew_boards, dEvals_comparison_results, boards_count, new_boards_count);
    else if (mode == ChessSolverMode::GPU) copy_selected_boardsGPU(dBoards, dEvals_to_filter, dNew_boards, dEvals_comparison_results, boards_count, new_boards_count);
    else null_mode_exit();
    if (debug) {
        cout << "COPY SELECT" << endl;
        debug_N_print<pc, uint>(*dBoards, boards_count);
    }
}

void find_move_with_minimum_score(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count, uchar* output_moves) {
    if (mode == ChessSolverMode::CPU) find_move_with_minimum_scoreCPU(dBoards, dSource_moves, dEvals_black, boards_count, output_moves);
    else if (mode == ChessSolverMode::GPU) find_move_with_minimum_scoreGPU(dBoards, dSource_moves, dEvals_black, boards_count, output_moves);
    else null_mode_exit();
    if (debug) {
        cout << "MIN-SC" << endl;
        //debug_print<uchar, uint>(output_moves, boards_count, 4);
    }
}


void clean_memory(pc* dNew_boards, uchar* dMoves_count_per_board, uint* dScan_results, bool* dEvals_comparison_results) {
    if (mode == ChessSolverMode::CPU) clean_memoryCPU(dNew_boards, dMoves_count_per_board, dScan_results, dEvals_comparison_results);
    else if (mode == ChessSolverMode::GPU) clean_memoryGPU(dNew_boards, dMoves_count_per_board, dScan_results, dEvals_comparison_results);
    else null_mode_exit();
}

void clean_memory(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count) {
    if (mode == ChessSolverMode::CPU) clean_memoryCPU(dBoards, dSource_moves, dEvals_black, boards_count);
    else if (mode == ChessSolverMode::GPU) clean_memoryGPU(dBoards, dSource_moves, dEvals_black, boards_count);
    else null_mode_exit();
}

template<typename T, typename Y>
void debug_print(T* arr, Y* count) {
    if (mode == ChessSolverMode::CPU) debug_printCPU<T,Y>(arr,count);
    else if (mode == ChessSolverMode::GPU) debug_printGPU<T, Y>(arr,count);
    else null_mode_exit();
}

template<typename T, typename Y>
void debug_print(T* arr, Y* count, int multiplier) {
    if (mode == ChessSolverMode::CPU) debug_printCPU<T, Y>(arr, count, multiplier);
    else if (mode == ChessSolverMode::GPU) debug_printGPU<T, Y>(arr, count, multiplier);
    else null_mode_exit();
}

template<typename T, typename Y>
void debug_N_print(T* arr, Y* count) {
    if (mode == ChessSolverMode::CPU) debug_N_printCPU<T, Y>(arr, count);
    else if (mode == ChessSolverMode::GPU) debug_N_printGPU<T, Y>(arr, count);
    else null_mode_exit();
}
