#pragma once
#include "gpu_kernels.cuh"

using namespace std;

template <typename T, typename Y>
void debug_printGPU(T* arr, Y* count);

int extract_blocks_count(int boards_count) {
    return (boards_count - (boards_count % TPB)) / TPB + 1;
}

int extract_blocks_count(uint boards_count) {
    return (boards_count - (boards_count % TPB)) / TPB + 1;
}

bool cudaContinue(char* msg) {
    cudaError_t __err = cudaGetLastError();
    if (__err != cudaSuccess) {

        fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n",
            msg, cudaGetErrorString(__err),
            __FILE__, __LINE__);
        fprintf(stderr, "*** FAILED - ABORTING\n");
        return false;
    }
    return true;
}

void init_constant_weightsGPU() {
    hP_scores_w = new int[NN] {
        0, 0, 0, 0, 0, 0, 0, 0,
            50, 50, 50, 50, 50, 50, 50, 50,
            10, 10, 20, 30, 30, 20, 10, 10,
            5, 5, 10, 25, 25, 10, 5, 5,
            0, 0, 0, 20, 20, 0, 0, 0,
            5, -5, -10, 0, 0, -10, -5, 5,
            5, 10, 10, -20, -20, 10, 10, 5,
            0, 0, 0, 0, 0, 0, 0, 0
    };
    hN_scores_w = new int[NN] {
        -50, -40, -30, -30, -30, -30, -40, -50,
            -40, -20, 0, 0, 0, 0, -20, -40,
            -30, 0, 10, 15, 15, 10, 0, -30,
            -30, 5, 15, 20, 20, 15, 5, -30,
            -30, 0, 15, 20, 20, 15, 0, -30,
            -30, 5, 10, 15, 15, 10, 5, -30,
            -40, -20, 0, 5, 5, 0, -20, -40,
            -50, -40, -30, -30, -30, -30, -40, -50
    };
    hB_scores_w = new int[NN] {
        -20, -10, -10, -10, -10, -10, -10, -20,
            -10, 0, 0, 0, 0, 0, 0, -10,
            -10, 0, 5, 10, 10, 5, 0, -10,
            -10, 5, 5, 10, 10, 5, 5, -10,
            -10, 0, 10, 10, 10, 10, 0, -10,
            -10, 10, 10, 10, 10, 10, 10, -10,
            -10, 5, 0, 0, 0, 0, 5, -10,
            -20, -10, -10, -10, -10, -10, -10, -20
    };
    hR_scores_w = new int[NN] {
        0, 0, 0, 0, 0, 0, 0, 0,
            5, 10, 10, 10, 10, 10, 10, 5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            -5, 0, 0, 0, 0, 0, 0, -5,
            0, 0, 0, 5, 5, 0, 0, 0
    };
    hQ_scores_w = new int[NN] {
        -20, -10, -10, -5, -5, -10, -10, -20,
            -10, 0, 0, 0, 0, 0, 0, -10,
            -10, 0, 5, 5, 5, 5, 0, -10,
            -5, 0, 5, 5, 5, 5, 0, -5,
            0, 0, 5, 5, 5, 5, 0, -5,
            -10, 5, 5, 5, 5, 5, 0, -10,
            -10, 0, 5, 0, 0, 0, 0, -10,
            -20, -10, -10, -5, -5, -10, -10, -20
    };
    hK_scores_w = new int[NN] {
        -30, -40, -40, -50, -50, -40, -40, -30,
            -30, -40, -40, -50, -50, -40, -40, -30,
            -30, -40, -40, -50, -50, -40, -40, -30,
            -30, -40, -40, -50, -50, -40, -40, -30,
            -20, -30, -30, -40, -40, -30, -30, -20,
            -10, -20, -20, -20, -20, -20, -20, -10,
            20, 20, 0, 0, 0, 0, 20, 20,
            20, 30, 10, 0, 0, 10, 30, 20
    };

    hP_scores_b = new int[NN];
    hN_scores_b = new int[NN];
    hB_scores_b = new int[NN];
    hR_scores_b = new int[NN];
    hQ_scores_b = new int[NN];
    hK_scores_b = new int[NN];


    for (int i = 0; i < NN; i++) {
        int x = i % N;
        int y_b = (i - x) / N;
        int y_w = N - y_b - 1;
        hP_scores_b[x + y_b * N] = hP_scores_w[x + y_w * N];
        hN_scores_b[x + y_b * N] = hN_scores_w[x + y_w * N];
        hB_scores_b[x + y_b * N] = hB_scores_w[x + y_w * N];
        hR_scores_b[x + y_b * N] = hR_scores_w[x + y_w * N];
        hQ_scores_b[x + y_b * N] = hQ_scores_w[x + y_w * N];
        hK_scores_b[x + y_b * N] = hK_scores_w[x + y_w * N];
    }

    cudaMemcpyToSymbol(dP_scores_w, hP_scores_w, NN * sizeof(int));
    cudaMemcpyToSymbol(dN_scores_w, hN_scores_w, NN * sizeof(int));
    cudaMemcpyToSymbol(dB_scores_w, hB_scores_w, NN * sizeof(int));
    cudaMemcpyToSymbol(dR_scores_w, hR_scores_w, NN * sizeof(int));
    cudaMemcpyToSymbol(dQ_scores_w, hQ_scores_w, NN * sizeof(int));
    cudaMemcpyToSymbol(dK_scores_w, hK_scores_w, NN * sizeof(int));

    cudaMemcpyToSymbol(dP_scores_b, hP_scores_b, NN * sizeof(int));
    cudaMemcpyToSymbol(dN_scores_b, hN_scores_b, NN * sizeof(int));
    cudaMemcpyToSymbol(dB_scores_b, hB_scores_b, NN * sizeof(int));
    cudaMemcpyToSymbol(dR_scores_b, hR_scores_b, NN * sizeof(int));
    cudaMemcpyToSymbol(dQ_scores_b, hQ_scores_b, NN * sizeof(int));
    cudaMemcpyToSymbol(dK_scores_b, hK_scores_b, NN * sizeof(int));
}

void copy_input_board_to_memoryGPU(pc* input_board, pc** dBoards) {
    cudaMalloc(dBoards, sizeof(pc) * NN1);
    cudaMemcpy(*dBoards, input_board, sizeof(pc) * NN, cudaMemcpyHostToDevice);
}


void evaluateGPU(pc* dBoards, ushort** dEvals, uint* boards_count) {
    cudaMalloc(dEvals, sizeof(ushort) * *boards_count);
    evaluate_kernel << <extract_blocks_count(*boards_count), TPB >> > (dBoards, *dEvals, *boards_count);
}



void black_count_all_movesGPU(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count) {
    uint* dMoves_sum;
    cudaMalloc(dMoves_count, sizeof(uchar) * *boards_count);
    cudaMalloc(&dMoves_sum, sizeof(uint) * *boards_count);

    count_moves_black_kernel << <extract_blocks_count(*boards_count), TPB >> > (dBoards, *dMoves_count, *boards_count);
    transfer_moves_to_sum_kernel << <extract_blocks_count(*boards_count), TPB >> > (*dMoves_count, dMoves_sum, *boards_count);
    
    uint remaining_elements_count = *boards_count;
    uint processed_elements_count_halved;

    while (remaining_elements_count > 1) {
        if (remaining_elements_count % 2 == 0) {
            processed_elements_count_halved = remaining_elements_count / 2;
            sumReduceEven << <extract_blocks_count(processed_elements_count_halved), TPB >> > (dMoves_sum, processed_elements_count_halved);
        }
        else {
            processed_elements_count_halved = (remaining_elements_count - 1) / 2;
            sumReduceOdd << <extract_blocks_count(processed_elements_count_halved), TPB >> > (dMoves_sum, processed_elements_count_halved);
        }
        remaining_elements_count -= processed_elements_count_halved;
    }
    cudaMemcpy(new_boards_count, dMoves_sum, sizeof(uint), cudaMemcpyDeviceToHost);
    cudaFree(dMoves_sum);
}


void white_count_all_movesGPU(pc* dBoards, uchar** dMoves_count, uint* boards_count, uint* new_boards_count) {
    uint* dMoves_sum;
    cudaMalloc(dMoves_count, sizeof(uchar) * *boards_count);
    cudaMalloc(&dMoves_sum, sizeof(uint) * *boards_count);

    count_moves_white_kernel << <extract_blocks_count(*boards_count), TPB >> > (dBoards, *dMoves_count, *boards_count);
    transfer_moves_to_sum_kernel << <extract_blocks_count(*boards_count), TPB >> > (*dMoves_count, dMoves_sum, *boards_count);

    uint remaining_elements_count = *boards_count;
    uint processed_elements_count_halved;

    while (remaining_elements_count > 1) {
        if (remaining_elements_count % 2 == 0) {
            processed_elements_count_halved = remaining_elements_count / 2;
            sumReduceEven << <extract_blocks_count(processed_elements_count_halved), TPB >> > (dMoves_sum, processed_elements_count_halved);
        }
        else {
            processed_elements_count_halved = (remaining_elements_count - 1) / 2;
            sumReduceOdd << <extract_blocks_count(processed_elements_count_halved), TPB >> > (dMoves_sum, processed_elements_count_halved);
        }
        remaining_elements_count -= processed_elements_count_halved;
    }
    cudaMemcpy(new_boards_count, dMoves_sum, sizeof(uint), cudaMemcpyDeviceToHost);
    cudaFree(dMoves_sum);
}


void allocate_memory_for_new_boardsGPU(pc** dNew_boards, uint* boards_count) {
    cudaMalloc(dNew_boards, sizeof(pc) * NN1 * *boards_count);
}

void black_make_all_movesGPU(pc* dNew_boards, pc* dBoards, uchar* moves_count, uint* new_boards_count, uint* boards_count, uchar** dSource_moves, uint** scan_results) {
    cudaMalloc(scan_results, sizeof(uint) * *boards_count);
    cudaMalloc(dSource_moves, sizeof(uchar) * 4 * *new_boards_count);

    uchar* dMoves_count_tmp;
    cudaMalloc(&dMoves_count_tmp, sizeof(uchar) * *boards_count);
    cudaMemcpy(dMoves_count_tmp, moves_count, sizeof(uchar) * *boards_count, cudaMemcpyDeviceToDevice);

    prescan_kernel<<<extract_blocks_count(*boards_count), TPB, 2 * TPB * sizeof(uint) >>>(*scan_results, dMoves_count_tmp, *boards_count);
    cudaFree(dMoves_count_tmp);


    make_all_black_moves_kernel<<<extract_blocks_count(*boards_count),TPB>>>(dNew_boards, dBoards, *scan_results, *dSource_moves, *boards_count);
}

void black_make_all_movesGPU(pc* dNew_boards, pc* dBoards, uchar* moves_count, uint* new_boards_count, uint* boards_count, uint** scan_results) {
    cudaMalloc(scan_results, sizeof(uint) * *boards_count);

    uchar* dMoves_count_tmp;
    cudaMalloc(&dMoves_count_tmp, sizeof(uchar) * *boards_count);
    cudaMemcpy(dMoves_count_tmp, moves_count, sizeof(uchar) * *boards_count, cudaMemcpyDeviceToDevice);
    
    prescan_kernel << <extract_blocks_count(*boards_count), TPB, 2 * TPB * sizeof(uint) >> > (*scan_results, dMoves_count_tmp, *boards_count);
    cudaFree(dMoves_count_tmp);

    make_all_black_moves_kernel << <extract_blocks_count(*boards_count), TPB >> > (dNew_boards, dBoards, *scan_results, *boards_count);
}

void white_make_all_movesGPU(pc* dNew_boards, pc* dBoards, uchar* moves_count, uint* new_boards_count, uint* boards_count, uint** scan_results) {
    cudaMalloc(scan_results, sizeof(uint) * *boards_count);

    uchar* dMoves_count_tmp;
    cudaMalloc(&dMoves_count_tmp, sizeof(uchar) * *boards_count);
    cudaMemcpy(dMoves_count_tmp, moves_count, sizeof(uchar) * *boards_count, cudaMemcpyDeviceToDevice);

    prescan_kernel << <extract_blocks_count(*boards_count), TPB, 2 * TPB * sizeof(uint) >> > (*scan_results, moves_count, *boards_count);
    cudaFree(dMoves_count_tmp);

    make_all_white_moves_kernel << <extract_blocks_count(*boards_count), TPB >> > (dNew_boards, dBoards, *scan_results, *boards_count);
}

void black_compareGPU(ushort* dEvals_black, ushort** dEvals_white, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results) {
    ushort* dEvals_white_extended;
    cudaMalloc(&dEvals_white_extended, sizeof(ushort) * *new_boards_count);
    extend_evaluations_kernel<<<extract_blocks_count(*boards_count),TPB>>>(dEvals_white_extended, *dEvals_white, dMoves_count, dScan_results, *boards_count);

    cudaFree(*dEvals_white);
    *dEvals_white = dEvals_white_extended;

    cudaMalloc(dEvals_comparison_results, sizeof(bool) * *new_boards_count);

    compare_black_kernel<<<extract_blocks_count(*new_boards_count),TPB>>>(dEvals_black, *dEvals_white, *dEvals_comparison_results, *new_boards_count);
}

void white_compareGPU(ushort* dEvals_white, ushort** dEvals_black, uint* new_boards_count, uint* boards_count, bool** dEvals_comparison_results, uchar* dMoves_count, uint* dScan_results) {
    ushort* dEvals_black_extended;
    cudaMalloc(&dEvals_black_extended, sizeof(ushort) * *new_boards_count);

    extend_evaluations_kernel<<<extract_blocks_count(*boards_count), TPB >>>(dEvals_black_extended, *dEvals_black, dMoves_count, dScan_results, *boards_count);

    cudaFree(*dEvals_black);
    *dEvals_black = dEvals_black_extended;

    cudaMalloc(dEvals_comparison_results, sizeof(bool) * *new_boards_count);
    compare_white_kernel<<< extract_blocks_count(*new_boards_count), TPB>>>(dEvals_white, *dEvals_black, *dEvals_comparison_results, *new_boards_count);
}


void copy_selected_boardsGPU(pc** dBoards, ushort** dEvals, pc* dNew_boards, bool* dEvals_comparison_results, uint* boards_count, uint* new_boards_count) {
    uint* comparisons_sum;
    cudaMalloc(&comparisons_sum, sizeof(uint) * *new_boards_count);

    transfer_comparisons_to_sum_kernel << <extract_blocks_count(*new_boards_count), TPB >> > (dEvals_comparison_results, comparisons_sum, *new_boards_count);

    uint remaining_elements_count = *new_boards_count;
    uint processed_elements_count_halved;

    while (remaining_elements_count > 1) {
        if (remaining_elements_count % 2 == 0) {
            processed_elements_count_halved = remaining_elements_count / 2;
            sumReduceEven << <extract_blocks_count(processed_elements_count_halved), TPB >> > (comparisons_sum, processed_elements_count_halved);
        }
        else {
            processed_elements_count_halved = (remaining_elements_count - 1) / 2;
            sumReduceOdd << <extract_blocks_count(processed_elements_count_halved), TPB >> > (comparisons_sum, processed_elements_count_halved);
        }
        remaining_elements_count -= processed_elements_count_halved;
    }

    uint new_dBoards_count;
    cudaMemcpy(&new_dBoards_count, comparisons_sum, sizeof(uint), cudaMemcpyDeviceToHost);
    cudaFree(comparisons_sum);

    if (new_dBoards_count == 0) {
        if (debug) cout << "!!!0 - OVERRIDE!!!" << endl;
        new_dBoards_count = *new_boards_count;
        set_comparisons_to_one_kernel<<<extract_blocks_count(*new_boards_count),TPB>>>(dEvals_comparison_results, *new_boards_count);
    }

    uint* scan_results;
    cudaMalloc(&scan_results, sizeof(uint) * *new_boards_count);

    bool* dEvals_comparison_results_tmp;
    cudaMalloc(&dEvals_comparison_results_tmp, sizeof(bool) * *new_boards_count);
    cudaMemcpy(dEvals_comparison_results_tmp, dEvals_comparison_results, sizeof(bool) * *new_boards_count, cudaMemcpyDeviceToDevice);

    prescan_kernel << <extract_blocks_count(*new_boards_count), TPB, 2 * TPB * sizeof(uint) >> > (scan_results, dEvals_comparison_results_tmp, *new_boards_count);
    cudaFree(dEvals_comparison_results_tmp);


    pc* new_dBoards;
    ushort* new_dEvals;
    cudaMalloc(&new_dBoards, sizeof(pc) * NN1 * new_dBoards_count);
    cudaMalloc(&new_dEvals, sizeof(ushort) * new_dBoards_count);

    copy_selected_boards_kernel<<<extract_blocks_count(*new_boards_count), TPB >>>(new_dBoards, new_dEvals, dNew_boards, *dEvals, dEvals_comparison_results, scan_results, *new_boards_count);

    cudaFree(scan_results);
    cudaFree(*dBoards);
    cudaFree(*dEvals);

    *dBoards = new_dBoards;
    *dEvals = new_dEvals;
    *boards_count = new_dBoards_count;
}

void find_move_with_minimum_scoreGPU(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count, uchar* moves) {
    
    if (*boards_count == 0) {
        printf("ERR: output boards_count is 0, random move will be played\n");
        moves[0] = dSource_moves[4 * 0 + 0];
        moves[1] = dSource_moves[4 * 0 + 1];
        moves[2] = dSource_moves[4 * 0 + 2];
        moves[3] = dSource_moves[4 * 0 + 3];
    }
    else {
        ushort* tmp_score = new ushort[1];

        cudaMemcpy(tmp_score, &dEvals_black[0], sizeof(ushort), cudaMemcpyDeviceToHost);
        int int_score = *tmp_score;

        int* score;
        uint* i;
        cudaMalloc(&score, sizeof(int));
        cudaMalloc(&i, sizeof(uint));
        cudaMemset(i, 0, sizeof(uint));
        cudaMemcpy(score, &int_score, sizeof(int), cudaMemcpyHostToDevice);

        find_minimum_score_kernel << <extract_blocks_count(*boards_count), TPB >> > (dEvals_black, *boards_count, score);
        find_minimum_scoring_move_kernel << <extract_blocks_count(*boards_count), TPB >> > (dBoards, dEvals_black, *boards_count, score, i);

        uint* i_out = new uint[1];
        cudaMemcpy(i_out, i, sizeof(uint), cudaMemcpyDeviceToHost);
        int* score_out = new int[1];
        cudaMemcpy(score_out, score, sizeof(int), cudaMemcpyDeviceToHost);
        cout << "SCORE: " << *score_out << endl;

        cudaMemcpy(moves, &dSource_moves[4 * *i_out], sizeof(uchar) * 4, cudaMemcpyDeviceToHost);
        cudaFree(score);
        cudaFree(i);
        delete(i_out);
    }
    cudaFree(dBoards);
    cudaFree(dSource_moves);
    cudaFree(dEvals_black);
}


void clean_memoryGPU(pc* dNew_boards, uchar* dMoves_count_per_board, uint* dScan_results, bool* dEvals_comparison_results) {
    cudaFree(dNew_boards);
    cudaFree(dMoves_count_per_board);
    cudaFree(dScan_results);
    cudaFree(dEvals_comparison_results);
}

void clean_memoryGPU(pc* dBoards, uchar* dSource_moves, ushort* dEvals_black, uint* boards_count) {
    cudaFree(dBoards);
    cudaFree(dSource_moves);
    cudaFree(dEvals_black);
    boards_count[0] = 1;
}

template <typename T, typename Y>
void debug_printGPU(T* arr, Y* count) {
    int c = 0;
    //cudaMemcpy(&c, count, sizeof(Y), cudaMemcpyDeviceToHost);
    c = (int)*count;
    printf("C: %d\n", c);
    for (int i = 0; i < c; i++) {
        T n = 0;
        cudaMemcpy(&n, &arr[i], sizeof(T), cudaMemcpyDeviceToHost);
        printf("%d ", n);
    }
    printf("\n");
}

template <typename T, typename Y>
void debug_printGPU(T* arr, Y* count, int m) {
    int c = 0;
    //cudaMemcpy(&c, count, sizeof(Y), cudaMemcpyDeviceToHost);
    c = (int)*count;
    printf("C: %d\n", c);
    for (int i = 0; i < c; i++) {
        for (int j = 0; j < m; j++) {
            T n = 0;
            cudaMemcpy(&n, &arr[i*m + j], sizeof(T), cudaMemcpyDeviceToHost);
            printf("%d ", n);
        }
        printf("\n");
    }
    printf("\n");
}

template <typename T, typename Y>
void debug_N_printGPU(T* arr, Y* count) {
    int c = 0;
    //cudaMemcpy(&c, count, sizeof(Y), cudaMemcpyDeviceToHost);
    c = (int)*count;

    printf("C: %d\n", c);
    for (int i = 0; i < c; i++) {
        for (int j = 0; j < NN1; j++) {
            T n = 0;
            cudaMemcpy(&n, &arr[i*NN1+j], sizeof(T), cudaMemcpyDeviceToHost);
            printf("%d ", n);
            if ((j+1) % N  == 0) printf("\n");
        }
        printf("\n\n");
    }
}