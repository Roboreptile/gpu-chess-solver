#pragma once
#include "consts_and_imports.cuh"

using namespace std;

void null_mode_exit() {
    cout << "Mode is null!" << endl;
    exit(EXIT_FAILURE);
}

void print_board(pc* board, int x, int y, int x_t, int y_t) {
    cout << "   0    1    2    3    4    5    6    7" << endl;
    for (int i = 0; i < NN; i++) {
        if (i % N == 0) cout << (NN - i) / N - 1 << "  ";
        else cout << "   ";
        pc piece = board[i];

        if (x_t + y_t * N == i) piece = board[x + y * N];
        if (x + y * N == i) piece = 0;
        
        switch (piece) {
        case king_white:
            cout << WHITE king;
            break;
        case queen_white:
            cout << WHITE queen;
            break;
        case rook_white:
            cout << WHITE rook;
            break;
        case bishop_white:
            cout << WHITE bishop;
            break;
        case knight_white:
            cout << WHITE knight;
            break;
        case pawn_white:
            cout << WHITE pawn;
            break;
        case king_black:
            cout << BLACK king;
            break;
        case queen_black:
            cout << BLACK queen;
            break;
        case rook_black:
            cout << BLACK rook;
            break;
        case bishop_black:
            cout << BLACK bishop;
            break;
        case knight_black:
            cout << BLACK knight;
            break;
        case pawn_black:
            cout << BLACK pawn;
            break;
        default:
            cout << "__";
            break;
        }
        if ((i + 1) % N == 0) cout << "  "<< (NN - i) / N <<endl;
    }
    cout << "   0    1    2    3    4    5    6    7" << endl;

}

void print_board(pc* board) {
    print_board(board, -1, -1, -1, -1);
}

bool valid_move(pc* board, int x, int y, int x_t, int y_t) {
    if (x < 0 || x > N - 1) return false;
    if (y < 0 || y > N - 1) return false;
    if (x_t < 0 || x_t > N - 1) return false;
    if (y_t < 0 || y_t > N - 1) return false;
    return true;
}


bool no_checkmate(pc* board) {
    bool kingWhite = false;
    bool kingBlack = false;
    for (int i = 0; i < NN; i++) {
        if (board[i] == king_white) kingWhite = true;
        if (board[i] == king_black) kingBlack = true;
    }

    if (!kingWhite && !kingBlack) cout << "What just happened?" << endl;
    else if (!kingWhite) cout << "Black won!" << endl;
    else if (!kingBlack) cout << "White won!" << endl;

    return kingWhite && kingBlack;
}

void user_move(pc* board) {
    print_board(board);

    int x = -1, y = -1, x_t = -1, y_t = -1;
    char decision = 0;
    while (decision != 'Y') {

        do {
            cout << "Original square: (x, y)" << endl;
            cin >> x >> y;
            cout << "Target square: (x, y)" << endl;
            cin >> x_t >> y_t;

            y = N - y - 1;
            y_t = N - y_t - 1;

        } while (!valid_move(board, x, y, x_t, y_t));

        print_board(board, x, y, x_t, y_t);

        cout << "Are you sure? (Y/N)" << endl;
        cin >> decision;
    }

    board[x_t + y_t * N] = board[x + y * N];
    board[x + y * N] = 0;

}

void play_move(pc* input_board, uchar* moves) {
    uchar x = moves[0];
    uchar y = moves[1];
    uchar x_new = moves[2];
    uchar y_new = moves[3];

    input_board[y_new * N + x_new] = input_board[y * N + x];
    input_board[y * N + x] = 0;
}

void print_time_per_depth(chrono::high_resolution_clock::time_point start, int current_depth) {
    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "Reached depth " << current_depth << " in " << duration.count() << " ms" << endl;
}


void print_computer_move(int x, int y, int x_n, int y_n) {
    y = N - y - 1;
    y_n = N - y_n - 1;
    printf("%d %d -> %d %d\n", x,y,x_n,y_n);
}